const config = require("config");
const https = require("https");
const {
    resolve
} = require("path");

/**
 * Send a "system" chat message. See https://owncast.online/api/latest/#tag/Integrations/paths/~1api~1integrations~1chat~1system/post
 * @param {String} text The text to be sent.
 */
exports.sendSystemMessage = function(text) {
    sendOwncastMessage(text, 'system');
}

/**
 * Send a standard chat message. See https://owncast.online/api/latest/#tag/Integrations/paths/~1api~1integrations~1chat~1send/post
 * @param {String} text The text to be sent.
 */
exports.sendChatMessage = function(text) {
    sendOwncastMessage(text, 'send');
}

/**
 * Check if user is a chat moderator.
 * @param {String} userId The id string identifying the user (not the nickname).
 * @returns {Promise.<undefined>} A promise that, when resolved, indicates whether the user is a modrator (true) or not (false).
 */
exports.isModerator = function(userId) {
    return new Promise((resolve, reject) => {
        const streamKey = config.get("owncast.stream_key");
        const options = {
            host: config.get("owncast.host"),
            path: "/api/admin/chat/users/moderators",
            method: "GET",
            headers: {
                Authorization: 'Basic ' + new Buffer.from('admin:' + streamKey).toString('base64')
            },
        };
        const mods = requestGet(options).then(
            (data) => {
                data = JSON.parse(data);
                let isMod = false;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].id === userId) {
                        isMod = true;
                        break;
                    }
                }
                resolve(isMod);
            },
            (error) => {
                reject(error);
            });
    });
}

/**
 * Use the owncast API to send a chat message.
 * See:
 *   - https://owncast.online/api/latest/#tag/Integrations/paths/~1api~1integrations~1chat~1send/post
 *   - https://owncast.online/api/latest/#tag/Integrations/paths/~1api~1integrations~1chat~1system/post
 * @param {String} text The text to be sent.
 * @param {String} type The type of message: "send" or "system".
 */
// TODO: enum or similar for "type" parameter.
function sendOwncastMessage(text, type) {
    const request_params = {
        host: config.get("owncast.host"),
        path: "/api/integrations/chat/" + type,
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + config.get("owncast.tokens.chat." + type),
        },
    };
    var message_json = {
        body: text,
    };
    // TODO error handling
    console.log(`-> Sending '${type}' chat message to owncast: "${text}"`);
    requestPost(request_params, JSON.stringify(message_json));
}

/**
 * 
 * @param {*} options 
 * @returns 
 */
// TODO this function is not owncast specific but a general wrapper for get requests. Move it elsewhere !?
function requestGet(options) {
    return new Promise((resolve, reject) => {
        const req = https.request(options, res => {
            console.log(`statusCode: ${res.statusCode}`);
            var data = "";
            res.on('data', d => {
                data += d;
            });
            res.on('end', function() {
                resolve(data);
            });
        });

        req.on('error', error => {
            reject(error);
        });

        req.end();
    });
}

/**
 * 
 * @param {*} options 
 * @param {*} data_to_send 
 */
// TODO this function is not owncast specific but a general wrapper for get requests. Move it elsewhere !?
// TODO return Promise
function requestPost(
    options,
    data_to_send
) {
    function OnResponse(response) {
        var data = "";
        response.on("data", function(chunk) {
            data += chunk; //Append each chunk of data received to this variable.
        });
        response.on("end", function() {
            console.log("server response: " + data); //Display the server's response, if any.
        });
    }
    var request = https.request(options, OnResponse); //Create a request object.
    request.write(data_to_send); //Send off the request.
    request.end(); //End the request.
}