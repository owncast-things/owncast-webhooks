const config = require("config");
const fs = require("fs");
const https = require("https");

/**
 * Request speech synthesis from IBM watson
 * @param {String} text The text to be spoken.
 * @param {String} outputFilename The filename to use to save the audio file sent by watson.
 * @param {String} voice A string identifying a IBM Watson voice. See https://cloud.ibm.com/docs/text-to-speech?topic=text-to-speech-voices#listVoices 
 */
// TODO Return promise instead of using a callback function.
exports.tts = async function(text, outputFilename, voice) {
    const apiKey = config.get("watson.api_key");
    const instanceId = config.get("watson.instance_id");
    const host = config.get("watson.host");
    const options = {
        host: host,
        path: `/instances/${instanceId}/v1/synthesize?voice=${voice}`,
        method: "POST",
        auth: `apikey:${apiKey}`,
        headers: {
            "Content-Type": "application/json",
            Accept: "audio/wav",
        },
    };
    const message = {
        text: text,
    };
    const data = await watsonSendRequest(options, JSON.stringify(message), outputFilename);

    return data;
}

/**
 * 
 * @param {Object} options Options for https.request. See https://nodejs.org/api/http.html#httprequestoptions-callback
 * @param {Object} dataToSend Request payload.
 * @param {String} outputFilename Filename where to save the data the server sends back.
 */
function watsonSendRequest(options, dataToSend, outputFilename) {
    return new Promise((resolve, reject) => {
        // What to do when receiving data back from watson
        function OnResponse(response) {
            // We put the data pieces in this array
            var data = [];
            response.on("data", function(chunk) {
                data.push(chunk);
            });

            response.on("end", function() {
                // When we got all the data, glue all the pieces together
                var buffer = Buffer.concat(data);
                output_file_path = config.get('webhooks.tts_audio_files_path') + `/${outputFilename}`;
                // save the wav file
                fs.writeFile(output_file_path, buffer, function(err) {
                    if (err) {
                        reject(err);
                    }
                    console.log(`File saved to ${output_file_path}`);
                });
                return_object = {
                    text: JSON.parse(dataToSend).text,
                    wav_filename: outputFilename
                }
                resolve(return_object);
            });
        }
        const request = https.request(options, OnResponse);
        request.on('error', error => {
            reject(error);
        });
        // send the request
        request.write(dataToSend);
        request.end();
    });
}