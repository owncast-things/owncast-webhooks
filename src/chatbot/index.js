const config = require("config");
const owncast = require("../owncast");
const watsonTts = require("../watson");
const {
    htmlToText
} = require('html-to-text');


exports.isFeatureEnabled = function(feature_name) {
    return global.enabledFeatures[feature_name];
}

exports.bot = function() {
    const commandList = getChatbotCommandList();
    var text = "";
    for (let [cmd, desc] of commandList) {
        text += "- **" + cmd + "**: " + desc + "\n"
    }
    owncast.sendChatMessage(text);
}

exports.details = function() {
    var text = "- Stream chaque dimanche, mardi et jeudi à 20h00." + "\n" +
        "- Retrouvez-moi sur le fediverse : https://cybre.space/fractal" + "\n" +
        "- Code source sur https://framagit.org/owncast-things";
    owncast.sendChatMessage(text);
}

exports.say = async function(command, text) {
    if (global.enabledFeatures.tts === true) {
        const voice = (command === "!dis") ? "fr-FR_ReneeV3Voice" : "en-US_KevinV3Voice";
        const data = await tts(text, voice);
        global.io.sockets.emit("new_tts", JSON.stringify(data));
    } else {
        owncast.sendChatMessage("Oops! TTS is off!");
    }
}

exports.mod = function(text, user_id) {
    owncast.isModerator(user_id).then((isMod) => {
        if (isMod === true) {
            // if the user who sent the command is a mod
            var subcommand = findChatbotModSubCommand(text);
            console.log(`Found moderator command: "${subcommand}"`);
            switch (subcommand) {
                case "list":
                    const subcommandList = getChatbotModSubCommandList();
                    var infoText = "";
                    for (let [cmd, desc] of subcommandList) {
                        infoText += "- **!mod " + cmd + "**: " + desc + "\n"
                    }
                    owncast.sendChatMessage(infoText);
                    break;
                case "tts on":
                case "tts off":
                    if (subcommand === "tts on") {
                        global.enabledFeatures.tts = true;
                        owncast.sendChatMessage("Text to speech is **enabled**. 💬");
                    } else {
                        global.enabledFeatures.tts = false;
                        owncast.sendChatMessage("Text to speech if **disabled**. 🤫");
                    }
                    break;
                case 'emojirace on':
                case 'emojirace off':
                    if (subcommand === "emojirace on") {
                        global.enabledFeatures.emojirace = true;
                        owncast.sendChatMessage("Emoji race is **enabled**. 🚗");
                    } else {
                        global.enabledFeatures.emojirace = false;
                        owncast.sendChatMessage("Emoji race is **disabled**. 🚧");
                    }
                    break;
            }
        } else {
            owncast.sendChatMessage("You're not a mod! 👻");
        }
    });
}

/**
 * Send request to IBM Watson Text to speech service, and save the resulting WAV file.
 * Finally, send the WAV URL to the client for autoplay.
 * @param {String} text The text to be synthesized.
 * @param {String} voice A string identifying a IBM Watson voice. See https://cloud.ibm.com/docs/text-to-speech?topic=text-to-speech-voices#listVoices 
 */
async function tts(text, voice) {

    const cleanText = cleanupText(text);

    // Don't send a request if text is comically short.
    if (cleanText.length < 3) {
        return false;
    }
    const outputFilename = Date.now() + ".wav";

    // IBM watson request
    const data = await watsonTts.tts(
        text,
        outputFilename,
        voice
    );
    return data;
}

/**
 * Sanitize user input text.
 * @param {String} text The text to be sanitized.
 * @returns A cleaned up version of the text.
 */
function cleanupText(text) {
    // Strip html so watson doesn't try to read it,
    // and also for security reasons.
    text = htmlToText(text);
    // Strip emojis so watson don't try to read them.
    text.replace(/\p{Emoji}/gu, '');
    return text;
}

/**
 * Lists all available chatbot commands along with their description.
 * @returns {Map} The list of commands.
 */
function getChatbotCommandList() {
    const commands = new Map();
    commands.set('!bot', 'View chatbot commands.');
    commands.set('!details', 'Stream details and schedule.');
    commands.set('!say', 'Text to speech (in English). e.g. `!say Hello there`');
    commands.set('!dis', 'Synthèse vocale (en français). Ex: `!dis Bonjour !`');
    commands.set('!race', 'Start or join an emoji race!');
    commands.set('!saverig', 'Save your emoji rig to use in races.');
    commands.set('!mod', 'Commands for moderators.');
    return commands;
}

/**
 * List subcommands available to mods.
 * @returns {Map} The list of subcommands
 */
function getChatbotModSubCommandList() {
    const commands = new Map();
    commands.set('list', 'List commands available to moderators.');
    commands.set('tts on', 'Turn on TTS.');
    commands.set('tts off', 'Turn off TTS.');
    commands.set('emojirace on', 'Turn on the emoji race.');
    commands.set('emojirace off', 'Turn off the emoji race.');
    return commands;
}

/**
 * Extract chatbot command from string (the command must be at the beginning of the string).
 * @param {String} messageBody The string in which to look for the chatbot command.
 * @param {Map} commandsList An optional Map of the commands we're looking for. If null, uses getChatbotCommandList().
 * @returns {(String|Null)} If a command is found: the command. If no command is found: null.
 */
function findChatbotCommand(messageBody, commandsList = null) {
    const commands = commandsList !== null ? commandsList : getChatbotCommandList();
    for (let [cmd, desc] of commands) {
        if (messageBody.startsWith(cmd)) {
            return cmd;
        }
    }
    return null;
}
exports.findChatbotCommand = findChatbotCommand;

/**
 * Extract moderators only chatbot command from string (the command must be at the beginning of the string).
 * @param {String} messageBody The string in which to look for the chatbot command.
 * @returns {(String|Null)} If a command is found: the command. If no command is found: null.
 */
function findChatbotModSubCommand(messageBody) {
    const commands = getChatbotModSubCommandList();
    return findChatbotCommand(messageBody, commands);
}